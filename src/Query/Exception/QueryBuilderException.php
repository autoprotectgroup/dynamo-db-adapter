<?php

namespace Autoprotect\DynamodbODM\Query\Exception;

/**
 * Class QueryBuilderException
 *
 * @package DealTrak\Adapter\DynamoDBAdapter\Query\Exception
 */
class QueryBuilderException extends \Exception
{

}
